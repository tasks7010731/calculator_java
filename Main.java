import java.util.*;

public class Main {
    public static int pow (int a, int b)
    {
        /*Raises a to the power of b*/
        if (b == 0)        return 1;
        if (b == 1)        return a;
        if (b % 2 == 0 )    return     pow ( a * a, b/2);
        else                return a * pow ( a * a, b/2);

    }

    public static String a_to_r(int arabic) throws NumberFormatException {
        /*Converts integer to roman number*/
        if (arabic >= 4000) throw new NumberFormatException("Numbers bigger than 3999 can't be converted to roman numbers");
        if (arabic <= 0) throw new NumberFormatException("Non-positive numbers can't be converted to roman numbers");
        String order = "IVXLCDM";
        StringBuilder roman = new StringBuilder();
        int pos = 0;
        while (arabic > 0) {
            int digit = arabic % 10;
            arabic /= 10;
            switch (digit) {
                case 0: break;
                case 1: roman.insert(0, order.charAt(pos)); break;
                case 2: roman.insert(0, String.valueOf(order.charAt(pos)).repeat(2)); break;
                case 3: roman.insert(0, String.valueOf(order.charAt(pos)).repeat(3)); break;
                case 4: roman.insert(0, String.valueOf(order.charAt(pos)) + order.charAt(pos + 1)); break;
                case 5: roman.insert(0, order.charAt(pos + 1)); break;
                case 6: roman.insert(0, String.valueOf(order.charAt(pos + 1)) + order.charAt(pos)); break;
                case 7: roman.insert(0, order.charAt(pos + 1) + String.valueOf(order.charAt(pos)).repeat(2)); break;
                case 8: roman.insert(0, order.charAt(pos + 1) + String.valueOf(order.charAt(pos)).repeat(3)); break;
                case 9: roman.insert(0, String.valueOf(order.charAt(pos)) + order.charAt(pos + 2)); break;
            }
            pos += 2;
        }
        return roman.toString();
    }
    public static int r_to_a(String roman) throws NumberFormatException {
        /*Converts roman number to integer*/
        if (roman.length() == 0) throw new NumberFormatException("Empty roman number");
        String order = "IVXLCDM";
        int[] values = new int[roman.length()];
        for (int i=0; i<roman.length(); i++) {
            int ind = order.indexOf(roman.charAt(i));
            if (ind == -1) {
                throw new NumberFormatException("Found illegal symbol in roman number");
            }
            else {
                values[i] = pow(10, ind / 2) * ((ind % 2) * 4 + 1); //Magic
                if (i > 1 && values[i - 2] < 0 && values[i] >= values[i - 1]) throw new NumberFormatException("Illegal roman number");
                if (i > 2 && values[i - 3] == values[i - 2] &&
                        values[i - 2] == values[i - 1] &&
                        values[i - 1] == values[i])
                    throw new NumberFormatException("Illegal roman number");
                if (i > 0 && values[i] > values[i - 1]) {
                    if (i > 1 && values[i - 2] == values[i - 1]) throw new NumberFormatException("Illegal roman number");
                    int check = values[i] / values[i - 1];
                    if (check > 10 || (check == 2)) throw new NumberFormatException("Illegal roman number");
                    values[i - 1] = -values[i - 1];
                } else if (i > 1 && values[i - 2] < 0 && - values[i] / values[i - 2] != 0) throw new NumberFormatException("Illegal roman number");
            }
        }
        return Arrays.stream(values).sum();
    }

    public static boolean isArabic(String strNum) {
        /*checks if number is arabic*/
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isRoman(String strNum) {
        /*checks if number is roman*/
        if (strNum == null) {
            return false;
        }
        try {
            int d = r_to_a(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static int apply_operation(int left, int right, char operation) {
        /*Applies operation to operands*/
        if (left < 0 || right < 0) throw new ArithmeticException("Numbers should be non-negative");
        if (left > 10 || right > 10) throw new ArithmeticException("Sorry, numbers bigger than 10, even tho supported, are forbidden");
        if (operation == '+') return left + right;
        if (operation == '-') return left - right;
        if (operation == '*') return left * right;
        if (operation == '/') return left / right;
        return 1;
    }

    public static String calc(String input) {
        /*Calculates expression*/
        char operation = '\0';
        String left = "";
        String right = "";
        String result;
        int ind;
        if ((ind = input.indexOf('+')) != -1) operation = '+';
        else if ((ind = input.indexOf('-')) != -1) operation = '-';
        else if ((ind = input.indexOf('*')) != -1) operation = '*';
        else if ((ind = input.indexOf('/')) != -1) operation = '/';
        else throw new ArithmeticException("Operation not found");
        left = String.valueOf(input.subSequence(0, ind)).trim();
        right = String.valueOf(input.subSequence(ind + 1, input.length())).trim();
        if (isArabic(left) && isArabic(right)) {
            int result_arabic = apply_operation(Integer.parseInt(left), Integer.parseInt(right), operation);
            result = String.valueOf(result_arabic);
        } else if (isRoman(left) && isRoman(right)) {
            int result_arabic = apply_operation(r_to_a(left), r_to_a(right), operation);
            result = a_to_r(result_arabic);
        } else throw new ArithmeticException("Wrong operands");
        return result;
    }

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        String input = sc.nextLine();
        String result = calc(input);
        System.out.println(result);
    }
}
